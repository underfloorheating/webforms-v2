# Vladimir Popov's Webforms V2

## 2.8.3
+ New: Multi-select option in Select type field added
+ New: Email stoplist regular expression validation added
- Fix: Magento 2 quick response save fixed

## 2.8.2
+ New: reCaptcha localization is set to Store View locale automatically
- Fix: Magento 2 incorrect locale date format fix
- Fix: Magento 2 fields localization fix
- Fix: Invisible fields backend validation fix
- Fix: Logically non-visible fields no longer store values on submission
- Fix: Logic function fields visibility fix in admin area
- Fix: Star rating now accepts 0 as default selected value
- Fix: Checkbox and radio fields now have per option CSS classes


## 2.8.1
- Fix: Magento 2 deleting uploaded files fix
- Fix: Field edit in Store View corrected
- Fix: Small code refinements

## 2.8.0
+ New: Form import / export functions
+ New: Approval controls improved
+ New: Magento 2 WCAG compatibility features introduced
- Fix: Security XSS fix
- Fix: Multi-step template fieldset logic triggering corrected
- Fix: Miscellaneous small fixes

## 2.7.9
- Fix: Magento 2 IE11 Ajax submission json file download dialog fixed
- Fix: Magento 2 Honeypot captcha re-programmed
- Fix: Magento 2 message center adjusted
- Fix: Grid export decorate status removed
- Fix: multi-store download link fixed
- Fix: Number field missing placeholder fixed

## 2.7.8
+ New: file upload function re-written from scratch
+ New: hidden field algorithm changed and no longer accepts PHP codes
+ New: auto-complete field type
- Fix: some specific validation for requied fields fix
- Fix: accept URL parameters fix for some field types

## 2.7.7
+ New: form can accept URL parameters to set field values
- Fix: disabled logic fix for notification emails
+ Security enhancements

## 2.7.6
+ New: Bcc parameter added to E-mail Settings
+ New: Leave form on a page after submission widget parameter added
+ New: Caption to select + image field added
+ New: PDF filename updated
+ New: Multistep logic improved
- Fix: Magento 2 custom role permissions algorithm fixed
+ Security enhancements

## 2.7.5
+ New Select field option attribute {{disabled}} added
- Fix: inactive logic fix
- Fix: Magento 2 backend field save scope redirection fixed
- Fix: Magento 2 customer area form result subject fix

## 2.7.4
+ Magento 1: New Bubbles template for Web-forms: Results widget added
+ Magento 1: New Web-forms: Results widget parameters added
- Fix: Custom sender name fix for reply message
- Fix: Magento 2 deploy production fix
- Fix: Magento 2 approval status fix
- Fix: Select radio/checkbox custom validation fix
- Fix: Magento 2 missing access denied template added
- Fix: Magento 2 form access cache mode fix

## 2.7.3
+ Frontend error messages added for forms with limited access permissions
+ Memory allocation for image resizing added
+ billing_address and shipping_address variables added to notifications and PDF templates
+ Sender name added to E-mail Settings tab of the form
+ Form key check switch added to Web-forms > Settings page
+ Compatibility improvements with full page cache
+ Captcha verification switched to curl instead of file_get_contents
- Fix: Magento 2 broken links fixes
- Fix: Magento 2 custom mail extensions compatibility improved
- Fix: Magento 2 acl tree form list fix
- Fix: Some backend logic simplified for Store View compatibility
- Fix: custom role new form permission fix
- Fix: fix for customer result view in customer edit page
- Fix: added missing variables to reply-to template

## 2.7.2
+ Survey mode fix
+ PHP 7 compatibility improvements

## 2.7.1
+ Magento 2: PHP 7 support added
- Magento 2: Store View configuration fixes
- Stability improvements

## 2.7.0

+ Magento 2.x version released
+ mPDF library is removed from the main package
- Fix for broken action links in backend
- Fix for PDF template variables / images issues
- Fix for Logic Store View configuration

## 2.6.9

+ Automatically assigned Customer ID based on e-mail input
- Fix for possible controller issues
- Increased backend controller security

## 2.6.8

+ Date of birth field
- Compatibility with patch SUPEE-6788
- Fix for status notification if changed from customer page
- Fix for inlinecss option in transaction email templates
- Fix for permissions on form duplication
- Fix for customer Web-forms Results tab in backend customer edit page
- Mass form duplication error fix
- Ability to use same form multiple times on the same page

## 2.6.7

- Fix for the form widget for third party popup extensions
- Fix for e-mail notification with db load balancer
- Fix for customer name in email notifications for registration form add-on
- Fix for logic function in certain email notification cases

## 2.6.6

+ Formkey check added for the frontend form widget
+ Performance improvements
- Multistep template fixed to show reCaptcha on the last page only
- Select fields fixed to allow 0 value
- Logic function fixes for backend result edit page
- Site scripting fix added for results widget

## 2.6.5

- Submit form from backend
- New configuration option to disable Preview
- reCaptcha now reports as required field before submission
- Fixed: inserting image in form description and success text

## 2.6.4

- Fixed: text and textarea fields fix for compatibility with customer registration form

## 2.6.3

+ French translation added
+ Quick response template variables added
+ Security patch

## 2.6.2

+ RegEx expression validation improved
- Fixed: Number field min/max values fixed
+ Customer PDF template settings added
+ Compatibility with CustomField added

## 2.6.1

+ Configurable customer area web-forms block title in settings page
+ Improved Honeypot Captcha template
+ New permission to manage hidden fields
- Fixed: Developer > JS compression mode
- Fixed: Validation regular expression global modifier bug
- Fixed: Compatibility with Magento 1.6 and earlier added

## 2.6.0

+ Customer list of submissions in customer admin page
+ Completed status added in to approval system
+ New form Access Settings option
+ Overall admin interface improvements
+ New Text / Password type field
+ New Google reCaptcha v2
+ Image resize option for Image Upload fields

## 2.5.0

+ Validation error message parameter added for required fields
+ Changed tooltip library to Opentip
+ Improved server license validation

## 2.4.9

+ Forward to specified e-mail address in results grid

## 2.4.8

+ Export result to PDF
+ JavaScript file validation in file upload fields
- Fixed: hint is checked before the form is submitted

## 2.4.7

+ E-mail customer on result approval/disapproval

## 2.4.6

+ Speed improvements / sql foreign keys added

## 2.4.5

+ Ability to add files to notification emails as attachments

## 2.4.2

+ Advanced email address validation through SMTP

## 2.3.6

+ Compact template for sidebar forms
